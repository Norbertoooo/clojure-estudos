(ns clojure-estudos.core (:gen-class))

;; Pensamentos
; na maioria das vezes não precisa se preocupar com tipagem
; existe a necessidade de ordem de criação de função, se chamar antes de declarar, não reconhece
; uso de def dentro de uma função é marcado como não reconhecido, já o uso de let parece tudo ok, o que faz sentido

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))

(defn exibir-descricao [nome]
  (println "--------- uso de" nome "-------------")
  )

(defn eh-par-ou-impar? [number]
  (if (= (mod number 2) 0)
    (println "Número:" number "é par e o tipo:" (type number))
    (println "Número:" number "é ímpar e o tipo:" (type number))
    )
  )

(defn numero-eh-impar-ou-par []
  (println "Digite um número: ")
  (let [numero (read-line)]
    (println "Você digitou o número:" numero)
    (println (type numero))
    (eh-par-ou-impar? (Integer/parseInt numero))
    )
  )

(defn uso-de-if-do [number]
  (if (= number 2)
    (do (println number)
        (eh-par-ou-impar? number)
        )
    (do (println "bloco de else: " number)
        (println "continuação do bloco de else")
        )
    )

  )

(defn uso-de-case [nome]
  (case nome
    "vitor" (println "vitor")
    "amanda" (println "amanda")
    2 (println "2")
    (println "desconhecido")
    )
  )

(defn uso-de-cond []
  (let [x 10]
    (cond
      (= x 5) (println "x = 5")
      (= x 10) (println "x = 10")
      :else (println x)
      )
    )
  )

(defn uso-de-multiplos-ifs [number]
  (if (and (> number 10) (< number 15))
    (println "numero entre 10 e 15")
    (println "numero fora de range"))
  )

(defn uso-de-while []
  (exibir-descricao "while")

  (def numero (atom 1))
  (while (<= @numero 20)
    (do
      (println numero)
      (swap! numero inc)
      )
    )
  )

(defn uso-de-doseq []
  (exibir-descricao "doseq")

  (def vetor [1 2 3 4 5])
  (doseq [item vetor]
    (println item)
    )
  )

(defn uso-de-dotimes []
  (exibir-descricao "dotimes")

  (dotimes [n 5]
    (println n)
    )
  )

(defn uso-de-loop []
  (exibir-descricao "loop")
  (loop [x 10]
    (when (> x 1)
      (println x)
      (recur (- x 2)))))

(defn -main []
  (foo "")
  (uso-de-multiplos-ifs 12)
  (uso-de-multiplos-ifs 20)
  (uso-de-if-do 2)
  (uso-de-cond)
  (uso-de-case "sdada")
  (uso-de-case 2)
  (uso-de-while)
  (uso-de-doseq)
  (uso-de-dotimes)
  (uso-de-loop)
  ;(numero-eh-impar-ou-par)
  )
